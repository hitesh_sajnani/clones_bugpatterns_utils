import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

class CategoryInfo {
    int bugs;
    double defectDensity;

}

class Summary {

    LinkedHashMap<String, CategoryInfo> CloneSummary;
    LinkedHashMap<String, CategoryInfo> NonCloneSummary;

}

public class ExcelReader {

    static final String DELIMINITER = ",";
    static final String SEPARATOR = ",";
    static final String SIGNAL = "&SUMMARY-START&";
    static LinkedHashMap<String, Summary> projectSummary = new LinkedHashMap<String, Summary>();

    public static void readOutputAndPopulateSummary(File output,
            String projectName) {

        BufferedReader br = null;
        String line = "";
        int count = 0;
        try {

            br = new BufferedReader(new FileReader(output));
            while ((line = br.readLine()) != null) {
                count++;
                String[] row = line.split(DELIMINITER);
              //  System.out.println("COUNT: " + count + line);
                if (row[0].contains(SIGNAL)) {
                //    System.out.println("STARTED");
                    br.readLine();
                    line = br.readLine();
                 //   System.out.println("LINE:  " + line);
                    row = line.split(DELIMINITER);
                    LinkedHashMap<String, CategoryInfo> cloneSummary = new LinkedHashMap<String, CategoryInfo>();
                    LinkedHashMap<String, CategoryInfo> nonCloneSummary = new LinkedHashMap<String, CategoryInfo>();

                    CategoryInfo totalNonClonesInfo = new CategoryInfo();
                    CategoryInfo totalClonesInfo = new CategoryInfo();
                  //  System.out.println(row[0]);
                 //   System.out.println(row[1]);
                    totalNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    totalClonesInfo.bugs = Integer.parseInt(row[3]);
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    totalNonClonesInfo.defectDensity = Double
                            .parseDouble(row[2]);
                    try{
                        totalClonesInfo.defectDensity = Double.parseDouble(row[3]);
                    }catch(NumberFormatException e){
                        totalClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("TOTAL", totalNonClonesInfo);
                    cloneSummary.put("TOTAL", totalClonesInfo);

                    br.readLine(); // CategoryWise,,,

                    // Style
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo styleNonClonesInfo = new CategoryInfo();
                    CategoryInfo styleClonesInfo = new CategoryInfo();
                    styleNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    styleClonesInfo.bugs = Integer.parseInt(row[3]);
                    styleNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        styleClonesInfo.defectDensity = Double.parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        styleClonesInfo.defectDensity = 0;
                    }
                    
                    
                    nonCloneSummary.put("STYLE", styleNonClonesInfo);
                    cloneSummary.put("STYLE", styleClonesInfo);

                    // BadPractice
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo badPracticeNonClonesInfo = new CategoryInfo();
                    CategoryInfo badPracticeClonesInfo = new CategoryInfo();
                    badPracticeNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    badPracticeClonesInfo.bugs = Integer.parseInt(row[3]);
                    badPracticeNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        badPracticeClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        badPracticeClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("BAD_PRACTICE",
                            badPracticeNonClonesInfo);
                    cloneSummary.put("BAD_PRACTICE", badPracticeClonesInfo);

                    // correctness
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo correctnessNonClonesInfo = new CategoryInfo();
                    CategoryInfo correctnessClonesInfo = new CategoryInfo();
                    correctnessNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    correctnessClonesInfo.bugs = Integer.parseInt(row[3]);
                    correctnessNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        correctnessClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        correctnessClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary
                            .put("CORRECTNESS", correctnessNonClonesInfo);
                    cloneSummary.put("CORRECTNESS", correctnessClonesInfo);

                    // II8N
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo I18NNonClonesInfo = new CategoryInfo();
                    CategoryInfo I18NClonesInfo = new CategoryInfo();
                    I18NNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    I18NClonesInfo.bugs = Integer.parseInt(row[3]);
                    I18NNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        I18NClonesInfo.defectDensity = Double.parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        I18NClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("I18N", I18NNonClonesInfo);
                    cloneSummary.put("I18N", I18NClonesInfo);

                    // Security
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo securityNonClonesInfo = new CategoryInfo();
                    CategoryInfo securityClonesInfo = new CategoryInfo();
                    securityNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    securityClonesInfo.bugs = Integer.parseInt(row[3]);
                    securityNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        securityClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        securityClonesInfo.defectDensity=0;
                    }
                    
                    nonCloneSummary.put("SECURITY", securityNonClonesInfo);
                    cloneSummary.put("SECURITY", securityClonesInfo);

                    // Performance
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo performanceNonClonesInfo = new CategoryInfo();
                    CategoryInfo performanceClonesInfo = new CategoryInfo();
                    performanceNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    performanceClonesInfo.bugs = Integer.parseInt(row[3]);
                    performanceNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        performanceClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        performanceClonesInfo.defectDensity=0;
                    }
                    
                    nonCloneSummary
                            .put("PERFORMANCE", performanceNonClonesInfo);
                    cloneSummary.put("PERFORMANCE", performanceClonesInfo);

                    // MT_Correctness
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo MT_CorrectnessNonClonesInfo = new CategoryInfo();
                    CategoryInfo MT_CorrectnessClonesInfo = new CategoryInfo();
                    MT_CorrectnessNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    MT_CorrectnessClonesInfo.bugs = Integer.parseInt(row[3]);
                    MT_CorrectnessNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        MT_CorrectnessClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        MT_CorrectnessClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("MT_CORRECTNESS",
                            MT_CorrectnessNonClonesInfo);
                    cloneSummary
                            .put("MT_CORRECTNESS", MT_CorrectnessClonesInfo);

                    // Experimental
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo experimentalNonClonesInfo = new CategoryInfo();
                    CategoryInfo experimentalClonesInfo = new CategoryInfo();
                    experimentalNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    experimentalClonesInfo.bugs = Integer.parseInt(row[3]);
                    experimentalNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        experimentalClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        experimentalClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("EXPERIMENTAL",
                            experimentalNonClonesInfo);
                    cloneSummary.put("EXPERIMENTAL", experimentalClonesInfo);

                    // maliciousCode
                    line = br.readLine();
                    row = line.split(DELIMINITER);
                    CategoryInfo maliciousCodeNonClonesInfo = new CategoryInfo();
                    CategoryInfo maliciousCodeClonesInfo = new CategoryInfo();
                    maliciousCodeNonClonesInfo.bugs = Integer.parseInt(row[2]);
                    maliciousCodeClonesInfo.bugs = Integer.parseInt(row[3]);
                    maliciousCodeNonClonesInfo.defectDensity = Double
                            .parseDouble(row[4]);
                    try{
                        maliciousCodeClonesInfo.defectDensity = Double
                                .parseDouble(row[5]);
                    }catch(NumberFormatException e){
                        maliciousCodeClonesInfo.defectDensity = 0;
                    }
                    
                    nonCloneSummary.put("MALICIOUS_CODE",
                            maliciousCodeNonClonesInfo);
                    cloneSummary.put("MALICIOUS_CODE", maliciousCodeClonesInfo);

                    Summary summary = new Summary();
                    summary.CloneSummary = cloneSummary;
                    summary.NonCloneSummary = nonCloneSummary;
                    projectSummary.put(projectName, summary);

                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static void dumpSummary(PrintWriter out) {

        // out.print("PROJECTNAME");

        List<String> categories = Arrays.asList("TOTAL", "STYLE",
                "BAD_PRACTICE", "CORRECTNESS", "I18N", "SECURITY",
                "PERFORMANCE", "MT_CORRECTNESS", "EXPERIMENTAL",
                "MALICIOUS_CODE");

        out.print("Project Name");

        for (String category : categories) {
            out.print(SEPARATOR + category + " BUGS_CLONES");
            out.print(SEPARATOR + category + " DD_CLONES");
        }

        for (String category : categories) {
            out.print(SEPARATOR + category + " BUGS_NONCLONES");
            out.print(SEPARATOR + category + " DD_NONCLONES");
        }

        out.println();

        System.out.println("SUMMARY: " + projectSummary);
        for (Entry<String, Summary> entry : projectSummary.entrySet()) {
            out.print(entry.getKey());
            out.print(SEPARATOR);
          //  System.out.println("ProjectName:  " + entry.getKey());
            Summary summary = entry.getValue();
          //  System.out.println("-----------CLONE-----------");

            HashMap<String, CategoryInfo> cloneSummary = summary.CloneSummary;
            for (Entry<String, CategoryInfo> entry1 : cloneSummary.entrySet()) {
                // System.out.print("CATEGORY:  "+entry1.getKey());
                CategoryInfo categoryInfo = entry1.getValue();
                out.print(categoryInfo.bugs);
                out.print(SEPARATOR);
                out.print(categoryInfo.defectDensity);
                out.print(SEPARATOR);
          //      System.out.print("   " + "BUGS:  " + categoryInfo.bugs);
          //      System.out.print("   " + "DD:  " + categoryInfo.defectDensity);
          //      System.out.println();
            }

            System.out.println("-----------NONCLONE-----------");

            HashMap<String, CategoryInfo> nonCloneSummary = summary.NonCloneSummary;
            for (Entry<String, CategoryInfo> entry1 : nonCloneSummary
                    .entrySet()) {
                // System.out.print("CATEGORY:  "+entry1.getKey());
                CategoryInfo categoryInfo = entry1.getValue();
                out.print(categoryInfo.bugs);
                out.print(SEPARATOR);
                out.print(categoryInfo.defectDensity);
                out.print(SEPARATOR);
                System.out.print("   " + "BUGS:  " + categoryInfo.bugs);
                System.out.print("   " + "DD:  " + categoryInfo.defectDensity);
                System.out.println();
            }

            out.println();
            out.flush();
        }

    }

    public static void main(String[] args) {

        for (String projectName : args) {
            System.out.println("Reading: " + projectName);
            String filename = "resources/" + projectName + "-clones_bugs.csv";
            System.out.println("filename: " + filename);
            File output = new File(filename);
            readOutputAndPopulateSummary(output, projectName);
            FileWriter fstream = null;
            try {
                fstream = new FileWriter("resources/SUMMARY.csv");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            PrintWriter out = new PrintWriter(fstream);
            dumpSummary(out);
            // Methods2Map.clear();
        }
    }
}
