package util;

public class BugCategories {
	
	public static final Integer TOTALBUGS = 0;
	public static final int STYLE = 1;
	public static final int BAD_PRACTICE = 2;
	public static final int CORRECTNESS = 3;
	public static final int I18N = 4;
	public static final int SECURITY = 5;
	public static final int PERFORMANCE = 6;
	public static final int MT_CORRECTNESS = 7;
	public static final int EXPERIMENTAL = 8;
	public static final int MALICIOUS_CODE = 9;
}
