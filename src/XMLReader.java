import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

class Bug {
	String bugType;
	String bugCategory;
	int bugPriority;
}

public class XMLReader {
	static HashMap<String, List<Bug>> Method2BugMap = new HashMap<String, List<Bug>>();
	final static String deliminiter = "~";

	static synchronized void addToMethod2BugMap(String methodName, Bug bug) {
		List<Bug> bugs = Method2BugMap.get(methodName);
		if (bugs == null) {
			bugs = new ArrayList<Bug>();
			bugs.add(bug);
			Method2BugMap.put(methodName, bugs);
		} else {
			bugs.add(bug);
		}
	}

	static void dumpMethod2BugMap(PrintWriter out) {

		List<String> categories = Arrays.asList("STYLE", "BAD_PRACTICE",
				"CORRECTNESS", "I18N", "SECURITY", "PERFORMANCE",
				"MT_CORRECTNESS", "EXPERIMENTAL", "MALICIOUS_CODE");
		out.print("Method Name");

		for (String category : categories) {
			out.print( deliminiter + category);
		}
		out.print(deliminiter + "TOTALBUGS");
		out.println();

		for (Entry<String, List<Bug>> entry : Method2BugMap.entrySet()) {
			out.print(entry.getKey());
			System.out.println(entry.getKey());
			int totalBugCount = 0;

			LinkedHashMap<String, Integer> bugCategoryCount = new LinkedHashMap<String, Integer>();
			for (String category : categories) {
				bugCategoryCount.put(category, 0);
			}
			for (Bug bug : entry.getValue()) {
				System.out.println("BUG C: " + bug.bugCategory);
				totalBugCount++;
				Integer count = bugCategoryCount.get(bug.bugCategory);
				count++;
				bugCategoryCount.put(bug.bugCategory, count);
			}

			System.out.println(bugCategoryCount);
			for (Entry<String, Integer> categoryEntry : bugCategoryCount
					.entrySet()) {
				out.print(deliminiter + categoryEntry.getValue());
			}
			out.print(deliminiter + totalBugCount);
			out.println();
			out.flush();
		}
		out.close();
	}

	static void parseXMLandPopulateMethod2Bug(File xmlFile) {
		SAXBuilder builder = new SAXBuilder();
		try {

			Document document = (Document) builder.build(xmlFile);
			Element rootNode = document.getRootElement();
			List<Element> bugInstances = rootNode.getChildren("BugInstance");
			for (Element bugInstance : bugInstances) {
				Bug bug = new Bug();
				bug.bugType = bugInstance.getAttribute("type").getValue();
				bug.bugCategory = bugInstance.getAttribute("category")
						.getValue();
				bug.bugPriority = Integer.parseInt(bugInstance.getAttribute(
						"priority").getValue());

				List<Element> methods = bugInstance.getChildren("Method");
				StringBuilder methodName = null;
				for (Element method : methods) {
					methodName = new StringBuilder(method
							.getAttribute("classname").getValue()
							.replaceAll("\\$", "."));
					methodName.append('.');
					methodName.append(method.getAttribute("name").getValue());
					System.out.println("method: " + methodName);
					addToMethod2BugMap(methodName.toString(), bug);
					break; // Only first method is where bug is surfaced -- other
							// methods are have different roles
				}
			}
		} catch (IOException io) {
			System.out.println(io.getMessage());
		} catch (JDOMException jdomex) {
			System.out.println(jdomex.getMessage());
		}
	}

	public static void main(String[] args) {

		for (String projectName : args) {
			System.out.println("Reading: " + projectName);
			File xmlFile = new File(
					"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\findbugs-"
							+ projectName + ".xml");
			FileWriter fstream = null;
			try {
				fstream = new FileWriter(
						"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\findbugs-"
								+ projectName + ".csv");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			PrintWriter out = new PrintWriter(fstream);

			parseXMLandPopulateMethod2Bug(xmlFile);
			dumpMethod2BugMap(out);
			Method2BugMap.clear();
		}
	}
}
