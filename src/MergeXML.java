import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import util.BugCategories;

class MethodInfo {
	int LOC;
	double defectDensity;
	int[] bugs = new int[10];
	// Integer bugs;
}

public class MergeXML {

	static LinkedHashMap<String, MethodInfo> Methods2Map = new LinkedHashMap<String, MethodInfo>();
	final static String deliminter = "~";

	public static  void updateHashMap(File file) {

		BufferedReader br = null;
		String line = "";

		try {

			br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {
				String[] row = line.split(deliminter);
				if (Methods2Map.containsKey(row[0])) {
					//System.out.println("buggy method: " + row[0]);
					MethodInfo methodInfo = Methods2Map.get(row[0]);
					//System.out.println(Integer.parseInt(row[10]));
					methodInfo.bugs[BugCategories.TOTALBUGS] = Integer
							.parseInt(row[10]);
					methodInfo.bugs[BugCategories.STYLE] = Integer
							.parseInt(row[1]);
					methodInfo.bugs[BugCategories.BAD_PRACTICE] = Integer
							.parseInt(row[2]);
					methodInfo.bugs[BugCategories.CORRECTNESS] = Integer
							.parseInt(row[3]);
					methodInfo.bugs[BugCategories.I18N] = Integer
							.parseInt(row[4]);
					methodInfo.bugs[BugCategories.SECURITY] = Integer
							.parseInt(row[5]);
					methodInfo.bugs[BugCategories.PERFORMANCE] = Integer
							.parseInt(row[6]);
					methodInfo.bugs[BugCategories.MT_CORRECTNESS] = Integer
							.parseInt(row[7]);
					methodInfo.bugs[BugCategories.EXPERIMENTAL] = Integer
							.parseInt(row[8]);
					methodInfo.bugs[BugCategories.MALICIOUS_CODE] = Integer
							.parseInt(row[9]);
					methodInfo.defectDensity = (double) (1.0 * methodInfo.bugs[BugCategories.TOTALBUGS])
							/ (methodInfo.LOC + 1);
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static  void readMethodsAndPopulateHashMap(File methodsSize) {

		BufferedReader br = null;
		String line = "";
		String separator = ",";
		try {

			br = new BufferedReader(new FileReader(methodsSize));
			while ((line = br.readLine()) != null) {
				String[] row = line.split(separator);
				MethodInfo methodInfo = new MethodInfo();
				methodInfo.LOC = Integer.parseInt(row[1]);
				Methods2Map.put(row[0], methodInfo);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	static  void  dumpMethod2BugMap(PrintWriter out) {

		// Print header
		List<String> categories = Arrays.asList("STYLE", "BAD_PRACTICE",
				"CORRECTNESS", "I18N", "SECURITY", "PERFORMANCE",
				"MT_CORRECTNESS", "EXPERIMENTAL", "MALICIOUS_CODE");

		out.print("Method Name");
		out.print(deliminter);
		out.print("LOC");

		for (String category : categories) {
			out.print(deliminter + category);
		}
		out.print(deliminter + "TOTALBUGS");
		out.print(deliminter + "DefectDensity");
		out.println();

		// Print content

		for (Entry<String, MethodInfo> entry : Methods2Map.entrySet()) {
			// Print methodname
			//System.out.println(entry.getKey());
			out.print(entry.getKey());
			out.print(deliminter);

			// Fetch method info and dump it
			MethodInfo methodInfo = entry.getValue();
			out.print(methodInfo.LOC);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.STYLE]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.BAD_PRACTICE]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.CORRECTNESS]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.I18N]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.SECURITY]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.PERFORMANCE]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.MT_CORRECTNESS]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.EXPERIMENTAL]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.MALICIOUS_CODE]);
			out.print(deliminter);
			out.print(methodInfo.bugs[BugCategories.TOTALBUGS]);
			out.print(deliminter);
			out.print(methodInfo.defectDensity);
			out.println();
			out.flush();
		}
		out.close();
		System.out.println("Finished");
	}

	public static void main(String[] args) {

		for (String projectName : args) {
			System.out.println("Reading: "+projectName);
			File methodsSize = new File(
					"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\methodSize-"
							+ projectName + ".csv");
			readMethodsAndPopulateHashMap(methodsSize);

			File methodBugs = new File(
					"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\findbugs-"
							+ projectName + ".csv");
			updateHashMap(methodBugs);

			FileWriter fstream = null;
			try {
				fstream = new FileWriter(
						"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\methodbugdensity-"
								+ projectName + ".csv");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			PrintWriter out = new PrintWriter(fstream);
			dumpMethod2BugMap(out);
			Methods2Map.clear();
		}
	}
}


