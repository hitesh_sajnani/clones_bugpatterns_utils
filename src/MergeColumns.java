import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


public class MergeColumns {
	static final int COLUMN_PACKAGE_NAME = 0;
	static final int COLUMN_TYPE_NAME = 1;
	static final int COLUMN_METHOD_NAME = 2;
	static int COLUMN_LOC;
	static final String DELIMINITER = ",";
	
	public static  void readMethodsAndDumpFile(String projectName) {
		
		File methodsSize = new File(
				"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\methods-"
						+ projectName + ".csv");
		
		FileWriter fstream = null;
		try {
			fstream = new FileWriter(
					"C:\\Users\\Hitesh\\workspace\\FindBugsXMLParser\\resources\\methodSize-"
							+ projectName + ".csv");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		PrintWriter out = new PrintWriter(fstream);
		
		BufferedReader br = null;
		String line = "";
		int index = 0;
		int count=0;
		try {

			br = new BufferedReader(new FileReader(methodsSize));
			String header = br.readLine(); // skip the header
			String headerColumns[] = header.split(DELIMINITER);
			count++;
			for (int i=0; i< headerColumns.length; i++)
			{
				if (headerColumns[i].equals("NOS"))
				{
					index = i+1;
					COLUMN_LOC = i;
					break;
				}
			}
			if(index == 0)
			{
				System.out.println("NOS MISSING");
				System.exit(1);
			}
			while ((line = br.readLine()) != null) {
				count++;
				String[] row = line.split(DELIMINITER);
				if(row.length < index)
				{
					continue;
				}
				if(row[COLUMN_TYPE_NAME].contains("$"))
				{
					row[COLUMN_TYPE_NAME] = row[COLUMN_TYPE_NAME].replace("$", ".");
					System.out.println(row[COLUMN_TYPE_NAME]);
				}
				if(row[COLUMN_TYPE_NAME].contains("(anonymous)"))
				{
					row[COLUMN_TYPE_NAME] = row[COLUMN_TYPE_NAME].replace("(anonymous)", "");
					System.out.println(row[COLUMN_TYPE_NAME]);
				}
				
				if(row[COLUMN_METHOD_NAME].equals("<init>"))
				{
					System.out.println(row[COLUMN_METHOD_NAME]);
					continue;	
				}
				System.out.println(row.length+"  COUNT: "+count);
				StringBuilder modifiedRow = new StringBuilder(row[COLUMN_PACKAGE_NAME]);
				modifiedRow.append(".").append(row[COLUMN_TYPE_NAME]).append(".").append(row[COLUMN_METHOD_NAME]).append(DELIMINITER).append(row[COLUMN_LOC]);
				out.println(modifiedRow);
				out.flush();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String[] args) {

		for (String projectName : args) {
			System.out.println("Reading: "+projectName);
			readMethodsAndDumpFile(projectName);
		}
	}
}
